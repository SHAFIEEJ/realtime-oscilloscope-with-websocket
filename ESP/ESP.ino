// Uncomment this block to make this program executable for both ESP8266 and ESP32 to compare their processing capability.

//#ifdef ESP8266
//  #include <ESP8266WebServer.h>
//  ESP8266WebServer server;
//#elif defined(ESP32)
//  #include <ESPAsyncWebServer.h>
//  ESPAsyncWebServer server;
//#else 
//  #error "Not using ESP32 or ESP8266 !!"
//#endif

#include <Arduino.h>

#include <WiFi.h>
#include <WiFiClient.h>
#include <FS.h>
#include <SPIFFS.h>
#include <WebSocketsServer.h>
#include <ESPAsyncWebServer.h>
  
// you need to set FORMAT_SPIFFS_IF_FAILED to true only for the first time
#define FORMAT_SPIFFS_IF_FAILED true
#define MIC 35

const char* ssid = "shafei";
const char* password = "s4445666";

AsyncWebServer server(80);
WebSocketsServer webSocket = WebSocketsServer(81);



String json ;
int sig = 0;

void webSocketEvent(uint8_t num, WStype_t type, uint8_t * payload, size_t length){
   if (type == WStype_TEXT){
    for(int i = 0; i < length; i++) Serial.print((char) payload[i]);
    Serial.println("W");
   }
}


void setup() {
    
  Serial.begin(115200);
  Serial.println("tryign to connect");

  // connecting to the WIFI network
  WiFi.begin(ssid, password);
  
  if(!SPIFFS.begin()){
    Serial.println("An Error has occurred while mounting SPIFFS");
    return;
  }

  while(WiFi.status()!=WL_CONNECTED) {
    Serial.print(".");
    delay(500);
  }
  Serial.println("");
  Serial.print("IP Address: ");
  Serial.println(WiFi.localIP());
  randomSeed(analogRead(0));
  
  // here we are creating our ESP web-server to host the html page
  server.on("/html", HTTP_GET, [](AsyncWebServerRequest *request){
    request->send(SPIFFS, "/html_page.html", "text/html");
  });

  server.begin();
  webSocket.begin();
  webSocket.onEvent(webSocketEvent);
  pinMode(MIC,INPUT);

}


void loop() {

  //The sound signal
  sig = analogRead(MIC);

  webSocket.loop();
  
  // I didn't use any JSON libraries here in order to reduce the proccessing time.
  json = "{\"value\":";
  json += sig;
  json += "}";

  webSocket.broadcastTXT(json.c_str(), json.length());
  
}
