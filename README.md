# ESP32 + Labview Realtime Oscilloscope Using WebSocket

## _First: Introduction and Potential Prospects_

The main two ideas behind this project is to compare the uneven capabilities of different hardware platforms (ESP32 and ESP8266) and to build a practical complete IOT system. I build a sound wave oscillator using ESP and LabView platform. A lot of present IOT applications are focusing on the realability and speed of multistage-transfering data amoung different hardware devices and software systems. This application serves the same idea by using the fastest protocol to transfer audio data. Despite the fact that the system could transfer data on 400Hz frequency, it failed to transfer audio in realtime as it needs at least 8000 Hz to represent a speech correctly. Yet, we can use it to transfer different types of data that don't need that much of speed in its transmission rate.

## _Second: ESP code_


![N|Solid](images/esp.PNG)


* The ESP meant to detect the analog samples (sound samples in this case) and then to send them to the specified webserver in JSON format through WS connection.
* It also serving the HTML page with its Javascript code (which collects the samples from the WS connection and calculates the sending rate and frequency and then plots the samples into a good-looking realtime line-chart).
* To make the code prettier, I used the "SPIFFS.h" library to separate the HTML code from the "C++" code and to store the HTML page inside the data folder.
* Don't forget to download and setup the required libraries for this project:
  - SPIFFS.h 
  - WebSocketsServer.h
  - ESPAsyncWebServer.h

 [This video](https://www.youtube.com/watch?v=Ijr6vep-XYQ&t=4s) helped me do so: 

## _Third: Labview program_


![N|Solid](images/labview.PNG)


* The Labview program is also meant to recive the samples from the WS in JSON format.
* Then, It can perform any kind of processing that suites our application (in this case I applied Fourier transform tusing spactrum plotting).
* Also, don't forget to download the required libraries and packages for this program:
  - [WebSockets v2.0.1.40](vipm://mediamongrels_ltd_lib_websockets_api?repo_url=http://ftp.ni.com/evaluation/labview/lvtn/vipm)
  - [JKI JSON](vipm://jki_lib_json_serialization?repo_url=http://www.jkisoft.com/packages)
  - [JKI serialization](vipm://jki_lib_serialization?repo_url=http://www.jkisoft.com/packages)
  - [JSON API](vipm://lava_lib_json_api?repo_url=http://www.jkisoft.com/packages)

------------------------------------------------------------------------

### This is a personal project, so if have any inquiry, don't hasitate to contact me on Linkedin:
* [Mohamad Shafiee Jaddini](https://www.linkedin.com/in/shafiee-jaddini/)
